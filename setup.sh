#!/bin/sh

# See https://docs.linuxserver.io/general/swag#nextcloud-subdomain-reverse-proxy-example for configuration options

# The following variable isn't currently implemented, but is meant to allow execution from a different director, allowing the setup-all.sh script to function.
SCRIPT_DIR=$(readlink -f "$0")
source ./.env &&

podman network create proxy-net

podman pod create \
  --network proxy-net \
  --name=nextcloud-pod

# Nextcloud needs to update its code to become functional with mariadb:latest, so for now, it is attached to 10.5
podman run --detach \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD="$MYSQL_PASSWORD" \
  --env MYSQL_ROOT_PASSWORD="$MYSQL_ROOT_PASSWORD" \
  --volume nextcloud-db:/var/lib/mysql:z \
  --pod=nextcloud-pod \
  --restart on-failure \
  --read-only=true \
  --name nextcloud-db \
  --pull always \
  docker.io/library/mariadb:10.5

podman run --detach \
  --pod=nextcloud-pod \
  --restart always \
  --read-only=true \
  --name nextcloud-redis \
  --pull always \
  docker.io/library/redis:alpine

podman run --detach \
  --env MYSQL_HOST="nextcloud-pod" \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD="$MYSQL_PASSWORD" \
  --env NEXTCLOUD_ADMIN_USER="$NEXTCLOUD_ADMIN_USER" \
  --env NEXTCLOUD_ADMIN_PASSWORD="$NEXTCLOUD_ADMIN_PASSWORD" \
  --env NEXTCLOUD_TRUSTED_DOMAINS="$SUBDOMAIN.$DOMAIN" \
  --env HTTP_X_FORWARDED_HOST="$SUBDOMAIN.$DOMAIN" \
  --env TRUSTED_PROXIES="proxy-pod.dns.podman" \
  --env OVERWRITEHOST="$SUBDOMAIN.$DOMAIN" \
  --env OVERWRITEPROTOCOL=https \
  --env REDIS_HOST="nextcloud-pod" \
  --env SMTP_HOST="$SMTP_HOST" \
  --env SMTP_SECURE="$SMTP_SECURE" \
  --env SMTP_PORT="$SMTP_PORT" \
  --env SMTP_AUTHTYPE="$SMTP_AUTHTYPE" \
  --env SMTP_NAME="$SMTP_NAME" \
  --env SMTP_PASSWORD="$SMTP_PASSWORD" \
  --env MAIL_FROM_ADDRESS="$MAIL_FROM_ADDRESS" \
  --env MAIL_DOMAIN="$MAIL_DOMAIN" \
  --volume nextcloud-app:/var/www/html:z \
  --volume nextcloud-data:/var/www/html/data:z \
  --pod=nextcloud-pod \
  --read-only-tmpfs=true \
  --restart on-failure \
  --name nextcloud \
  --pull always \
  docker.io/library/nextcloud:latest

podman run --detach \
  --name nextcloud-cron \
  --pod=nextcloud-pod \
  --read-only=true \
  --restart always \
  --volume nextcloud-app:/var/www/html:z \
  --volume nextcloud-data:/var/www/html/data:z \
  --entrypoint="/cron.sh" \
  docker.io/library/nextcloud:latest


