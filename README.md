[TOC]

## Quick Start

Clone this repository.

`cp .env.template .env`  
(Or copy the .env file from your oci-servers clone, if you are using that one, too.)

Edit `.env` with your information.

`sh setup.sh`

Proceed to setting up the oci-https-proxy with a Nextcloud config.

## Detailed Setup

For any more information, please see the nextcloud docker repository: https://github.com/docker-library/docs/blob/master/nextcloud/README.md
Clone this repository:  
`git clone https://gitlab.com/CloudArtAdmins/oci-nextcloud.git` 

Go into the repository's folder and copy the `.env.template` file:  
`cp .env.template .env`

Fill in the variables with your corrisponding values.  
Example:  
```
SUBDOMAIN=nextcloud   
MYSQL_PASSWORD='1 Good passWd.'  
MYSQL_ROOT_PASSWORD='2nd Even Better passWord!'    
NEXTCLOUD_ADMIN_USER=AdminUsername  
NEXTCLOUD_ADMIN_PASSWORD='Password for initial administrator login'   
REDIS_HOST_PASSWORD="if you have one."  
SMTP_HOST='smtp.yourMail.provider'  
SMTP_SECURE=ssl  
SMTP_PORT=465  
SMTP_AUTHTYPE=PLAIN  
SMTP_NAME='your@email.domain'  
SMTP_PASSWORD='password to the SMTP email'  
MAIL_FROM_ADDRESS='addressToSend@mail.From'    
```
NOTE: You do not need to give every variable a value. This will still start, even without all the variables of the template filled, although it may not function as desired without the correctly filled information. 

Run the setup script while in the same directory as the .env file. 

`sh setup.sh`  

Upon completion, everything should be funtioning. 

You should be able to check the containers by viewing their status or by listing them.  
They should be "up" for more than a couple of seconds.  
```
podman ps  
podman container ls -a
```

## Troubleshooting
In general when running the script for the first time, my recommended method of troubleshooting is to view the individual container's logs.  
The nextcloud container itself: `podman logs nextcloud`  
The nextcloud database container: `podman logs nextcloud-db`   
The cron container for nextcloud: `podman logs nextcloud-cron`  
The redis container for nextcloud: `podman logs nextcloud-redis`  

NOTE: Most of the time, the errors are on `nextcloud`

#### Misconfigured something on first run, so it isn't all automated...
I'd recommend just removing everything and trying again with the proper configurations:
```
podman pod stop nextcloud-pod
podman pod rm nextcloud-pod
podman volume rm nextcloud-data nextcloud-app nextcloud-db
sh setup.sh
```

Alternatively, you can fix the configurations and apply the correct information in the GUI after startup and/or in nextcloud's `/var/www/html/config/config.php`


#### Nextcloud won't start for longer than a few seconds.

I have found this to occur most when 
* The password to the maria/mysql database was incorrectly entered by nextcloud
* when nextcloud already did initial setup without connecting to the database
* Nextcloud does not have the correct password that it is trying to connect with
* Nextcloud is not connecting to the database correctly 
  * For example: podman's dns-plugin changed at one point, and the internal dns for these containers needed to be changed

(These issues would mean it is most likely the fault of my script, not you, but here is the information I have for troubleshooting if you need it.)

#### Nextcloud says .ocdata does not exist (or is inaccessible)
This is often a permissions or volume placement issue.  

If your file exists, it is in `~/.local/share/containers/storage/volumes/nextcloud-data/_data/`

You can try changing your file's ownership back to www-data, as it should be for all of this container's volumes:  
`podman exec -u www-data:www-data chown -R www-data:www-data /var/www/html/`  

Make sure that your nextcloud-cron container has access to the nextcloud-data folder.  
The setup script handles this by default, however if you made any modifications or if the container is down, this may be the problem.

#### Containers exit after closing my session/loging out.

This is assuming you want to run the script and keep the containers running.  
The alternative, probably more secure solution is to use a systemd unit, but assuming you don't want to do that....

As of systemd 230, you need to change your the default value of logind's variable `KillUserProcesses` to `no`.  
If you have the template of systemd-logind's configurations, you may just need to remove a comment `#`.

Edit the config file: `vim /etc/systemd/logind.conf`  
Add: `KillUserProcesses=no`   
Restart the service to apply configs: `sudo systemctl restart systemd-logind`  

Source: https://lwn.net/Articles/690166/

